package poczatek.pierwszaCzesc.pierwszaCzesc;

public class Osoba {
    String imie;
    int rokUrodzenia=28;
    boolean plec=false;

    public Osoba(String imie, int rokUrodzenia,boolean plec) {
        this.imie = imie;
        this.rokUrodzenia = rokUrodzenia;
        this.plec= true;
    }
    public Osoba(){}

    public static void main(String[] args) {




        Osoba Dawid=new Osoba();
        Dawid.imie="Dawid";
        Dawid.rokUrodzenia=28;
        Dawid.plec=false;

        Osoba Paweł=new Osoba();
        Paweł.imie="Paweł";
        Paweł.rokUrodzenia=28;
        Paweł.plec=false;
        Osoba Klaudia=new Osoba();
        Klaudia.imie="Klaudia";
        Klaudia.rokUrodzenia=28;
        Klaudia.plec=true;


        Osoba Marek = new Osoba("Marek", 33,true);

        Osoba[] wszyscy=new Osoba[]{Dawid,Paweł,Klaudia,Marek};

        for (int i = 0; i <wszyscy.length ; i++) {
            wszyscy[i].przedstawSie();



        }

        Dawid.przedstawSie();
        Paweł.przedstawSie();
        Klaudia.przedstawSie();

    }

    private void przedstawSie(){
        int wiek=2019-rokUrodzenia;
        System.out.printf("Siemka jestem %s"+" i mam %d lat\n",imie,rokUrodzenia);

    }
}
