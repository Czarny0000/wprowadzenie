package poczatek.pierwszaCzesc.pierwszaCzesc.menadzerSal;

public class Main {
    public static void main(String[] args) {
        Sala gdansk = new Sala();
        gdansk.nazwa = "Gdansk";
        gdansk.iloscM2 = 50.02;
        gdansk.liczbaStanowisk = 8;
        gdansk.rzutnik = true;


        Sala sztukaWyboru = new Sala();
        sztukaWyboru.nazwa = "SztukaW";
        sztukaWyboru.iloscM2 = 45;
        sztukaWyboru.liczbaStanowisk = 12;
        sztukaWyboru.rzutnik = false;

        Sala Nowa = new Sala("Nowa",100.02,30,false);
        Sala Sopot = new Sala("Sopot",120.02,40,true);
        Sala Gdynia = new Sala("Gdynia",80.02,20,true);

        Sala[] zarzadzaneSale = new Sala[]{gdansk,sztukaWyboru,Nowa,Sopot,Gdynia};

        Menadzer Jan = new Menadzer();
        Jan.imie = "Jan";
        Jan.sale = zarzadzaneSale;


        Jan.wyswietlDostepneSale();
        Jan.zabookujSale("Gdansk");
        Jan.wyswietlDostepneSale();
    }
}



