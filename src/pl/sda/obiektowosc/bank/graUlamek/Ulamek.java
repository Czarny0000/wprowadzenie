package pl.sda.obiektowosc.bank.graUlamek;

public class Ulamek {
    private int licznik;
    private int mianownik;

    public Ulamek(int licznik, int mianownik) {
        this.licznik = licznik;
        this.mianownik = mianownik;
    }

    public void wyswietl() {
        System.out.println(String.format("%d/%d", licznik, mianownik));
    }

    public Ulamek pomnoz(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.licznik;
        int mianownik = this.mianownik * ulamek.mianownik;

        return new Ulamek(licznik, mianownik);
    }

    public Ulamek podziel(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.licznik;
        int mianownik = this.mianownik * ulamek.mianownik;

        return new Ulamek(licznik, mianownik);

    }
}


