package domowe.kolejnyZbior;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Lotto {
    public static void main(String[] args) {
        int[] typowaneLiczby = new int[6];
        typujLicznyUzytkownika(typowaneLiczby);

        System.out.println(Arrays.toString(typowaneLiczby));

        int[] wylosowanieLiczby= new int[6];
        losujLiczby(wylosowanieLiczby);
        System.out.println("Wyslosowane liczby to:\n"+Arrays.toString(wylosowanieLiczby));
        int wynik= obliczTrafienia(typowaneLiczby,wylosowanieLiczby);

        System.out.println(String.format("Trafiłeś %d z %d liczb", wynik, 6));
    }

    private static int obliczTrafienia(int[] typowaneLiczby, int[] wylosowanieLiczby) {
        int liczbaTrafien=0;


        for (int i = 0; i <wylosowanieLiczby.length ; i++) {
            if (trafiona(typowaneLiczby,wylosowanieLiczby[i])){
                liczbaTrafien++;
            }

        }

        return liczbaTrafien;
    }

    private static boolean trafiona(int[] typowaneLiczby, int wylosowanaLiczba) {

        for(int typowanaLiczba:typowaneLiczby) {
            if(typowanaLiczba==wylosowanaLiczba)
                return true;
        }
        return false;
    }

    private static void losujLiczby(int[] liczby) {
        Random random= new Random();
        for (int i = 0; i <6; i++) {
            liczby[i]=random.nextInt(48)+1;

        }
    }

    private static void typujLicznyUzytkownika(int[] typowaneLiczby) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("podaj 6 liczb");
//        for (int j = 0; j < 6; j++) {
//            typowaneLiczby[j] = scanner.nextInt();
//
//        }
//    }
//}
        int wytypowaneLiczby = 0;
        int typowanaLiczba;
        do {
            System.out.println("podaj liczbe # " + wytypowaneLiczby);
            typowanaLiczba = scanner.nextInt();
            if (czyUnikatowa(typowaneLiczby, typowanaLiczba)&& typowanaLiczba>0 &&typowanaLiczba<=49) {
                typowaneLiczby[wytypowaneLiczby]=typowanaLiczba;wytypowaneLiczby++;
            }

        } while (wytypowaneLiczby < 6);
    }

    private static boolean czyUnikatowa(int[] typowaneLiczby, int typowanaLiczba) {
        return !trafiona(typowaneLiczby,typowanaLiczba);
    }
}


